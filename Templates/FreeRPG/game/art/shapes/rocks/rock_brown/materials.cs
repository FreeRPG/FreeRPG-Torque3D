




singleton Material(rock_brown_01)
{
   mapTo = "rock_brown_01";
   diffuseColor[0] = "0.796079 0.756863 0.635294 1";
   diffuseMap[0] = "art/terrains/rock/rock_brown_cracks_base.dds";
   normalMap[0] = "art/terrains/rock/rock_brown_cracks_normal.dds";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "16";
   specularMap[0] = "art/terrains/rock/rock_brown_cracks_detail.dds";
   subSurfaceColor[0] = "0.372549 0.32549 0.215686 1";
   subSurfaceRolloff[0] = "0.5";
   translucentBlendOp = "None";
   materialTag0 = "Rock";
};
