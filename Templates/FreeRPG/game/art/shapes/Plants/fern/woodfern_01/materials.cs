
singleton Material(woodfern_01)
{
   mapTo = "woodfern_01";
   diffuseColor[0] = "0.8 0.8 0.8 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "16";
   doubleSided = "1";
   translucent = "0";
   diffuseMap[0] = "art/shapes/plants/fern/woodfern_01/woodfern_01_diffuse.dds";
   alphaTest = "1";
   alphaRef = "80";
   normalMap[0] = "art/shapes/plants/fern/woodfern_01/woodfern_01_normal.dds";
   specularMap[0] = "art/shapes/plants/fern/woodfern_01/woodfern_01_specular.dds";
   materialTag0 = "Leaf";
   materialTag1 = "Plant";
};
