
singleton Material(broadfern_01_bunch_a_broadfern_01)
{
   mapTo = "broadfern_01";
   diffuseColor[0] = "0.835294 0.862745 0.705882 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucent = "0";
   diffuseMap[0] = "art/shapes/plants/fern/broadfern_01/broadfern_01_diffuse.dds";
   alphaTest = "1";
   alphaRef = "60";
   normalMap[0] = "art/shapes/plants/fern/broadfern_01/broadfern_01_normal.dds";
};
