
singleton TSShapeConstructor(Hanginglights_lightsDAE)
{
   baseShape = "./hanginglights_lights.DAE";
   adjustCenter = "1";
   adjustFloor = "1";
   loadLights = "0";
};
