
singleton Material(WorkBench_01_WorkBenchWood_horiz)
{
   mapTo = "WorkBenchWood_horiz";
   diffuseMap[0] = "art/shapes/3TD_Furniture_Pack_01/WorkBench_01/3TD_WoodGrain_15";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
   diffuseColor[0] = "0.729 0.729 0.729 1";
};

singleton Material(WorkBench_01_NailTextures)
{
   mapTo = "NailTextures";
   diffuseMap[0] = "3TD_MetalBrass_02";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
};

singleton Material(WorkBench_01_WorkBenchWood_vert)
{
   mapTo = "WorkBenchWood_vert";
   diffuseMap[0] = "art/shapes/3TD_Furniture_Pack_01/WorkBench_01/3TD_WoodGrain_15a";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
   diffuseColor[0] = "0.729412 0.729412 0.729412 1";
};
