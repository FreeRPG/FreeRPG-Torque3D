
singleton Material(BasicBookShelf_01_ShelfSupport_01)
{
   mapTo = "ShelfSupport_01";
   diffuseColor[0] = "0.0862745 0.0862745 0.0862745 1";
   specular[0] = "0.921569 0.921569 0.921569 1";
   specularPower[0] = "19";
   translucentBlendOp = "None";
   specularStrength[0] = "1.07843";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(BasicBookShelf_01_shelf_wood)
{
   mapTo = "shelf_wood";
   diffuseMap[0] = "3TD_WoodGrain_08";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "64";
   translucentBlendOp = "None";
   specularStrength[0] = "5";
   pixelSpecular[0] = "1";
   doubleSided = "1";
};

singleton Material(BasicBookShelf_01_ColColor)
{
   mapTo = "ColColor";
   diffuseColor[0] = "0.588 0.588 0.588 1";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
};
