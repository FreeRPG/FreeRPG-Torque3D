
singleton Material(EndTable_HR_01_FurnitureWoodHorz)
{
   mapTo = "FurnitureWoodHorz";
   diffuseMap[0] = "3TD_WoodGrain_11";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "106";
   translucentBlendOp = "None";
   specularStrength[0] = "0.588235";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(EndTable_HR_01_FurnitureWoodVert)
{
   mapTo = "FurnitureWoodVert";
   diffuseMap[0] = "3TD_WoodGrain_11a";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "21";
   translucentBlendOp = "None";
   pixelSpecular[0] = "1";
   doubleSided = "1";
};
