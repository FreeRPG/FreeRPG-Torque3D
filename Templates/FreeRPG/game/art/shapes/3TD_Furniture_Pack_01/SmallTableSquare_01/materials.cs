
singleton Material(SmallTableSquare_01_EndTableWood_horz)
{
   mapTo = "EndTableWood_horz";
   diffuseMap[0] = "3TD_WoodGrain_11";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "128";
   translucentBlendOp = "None";
   specularStrength[0] = "0.196078";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(SmallTableSquare_01_EndTableWood_vert)
{
   mapTo = "EndTableWood_vert";
   diffuseMap[0] = "3TD_WoodGrain_11a";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "69";
   translucentBlendOp = "None";
   specularStrength[0] = "0.392157";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
