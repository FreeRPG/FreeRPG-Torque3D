
singleton Material(EndTable_02sq_TableWood_horz)
{
   mapTo = "TableWood_horz";
   diffuseMap[0] = "3TD_WoodGrain_11";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "71";
   translucentBlendOp = "None";
   specularStrength[0] = "0.196078";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(EndTable_02sq_TableWood_vert)
{
   mapTo = "TableWood_vert";
   diffuseMap[0] = "3TD_WoodGrain_11a";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "24";
   translucentBlendOp = "None";
   specularStrength[0] = "5";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
