
singleton TSShapeConstructor(Mediaeval-house)
{
   baseShape = "./mediaeval-house.dae";
   adjustCenter = "1";
   adjustFloor = "1";
   loadLights = "0";
};
