
singleton Material(ladder1_Granite)
{
   mapTo = "Granite";
   diffuseColor[0] = "0.018919 0.018919 0.018919 1";
   specular[0] = "0.159363 0.161516 0.161516 1";
   specularPower[0] = "118";
   doubleSided = "1";
   translucentBlendOp = "None";
};
