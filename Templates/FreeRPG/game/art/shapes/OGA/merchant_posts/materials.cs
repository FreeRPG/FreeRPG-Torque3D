
singleton Material(merchant_post_aple)
{
   mapTo = "aple";
   diffuseMap[0] = "art/shapes/OGA/merchant_posts/manzana 2 texture";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(merchant_post_merc_prod_box)
{
   mapTo = "merc-prod-box";
   diffuseMap[0] = "art/shapes/OGA/merchant_posts/cajon con productos texture 3";
   normalMap[0] = "art/shapes/OGA/merchant_posts/cajon con productos texture 3 nmap";
   specularPower[0] = "50";
   specularMap[0] = "art/shapes/OGA/merchant_posts/cajon con productos texture 3 nmap";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(merchant_post_post_fabric)
{
   mapTo = "post-fabric";
   diffuseMap[0] = "art/shapes/OGA/merchant_posts/lona del puesto de mercader texture 4";
   normalMap[0] = "art/shapes/OGA/merchant_posts/lona del puesto de mercader texture 3 nmap";
   specularPower[0] = "50";
   specularMap[0] = "art/shapes/OGA/merchant_posts/lona del puesto de mercader texture 3 nmap";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(merchant_post_merc_table)
{
   mapTo = "merc-table";
   diffuseColor[1] = "White";
   diffuseMap[0] = "art/shapes/OGA/merchant_posts/puesto de mercader texture 5";
   normalMap[0] = "art/shapes/OGA/merchant_posts/puesto de mercader texture nmap";
   specularPower[0] = "50";
   specularMap[0] = "art/shapes/OGA/merchant_posts/puesto de mercader texture nmap";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(merchant_post2_bag)
{
   mapTo = "bag";
   diffuseColor[0] = "0.0681617 0.0734946 0.125943 1";
   specular[0] = "0.0307692 0.0307692 0.0307692 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post2_apple)
{
   mapTo = "apple";
   diffuseColor[0] = "0.263831 0.841189 0.193188 1";
   specular[0] = "0.473871 0.473871 0.473871 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post2_tent_cloth)
{
   mapTo = "tent_cloth";
   diffuseMap[0] = "whitelcoth";
   normalMap[0] = "whitelcoth";
   specular[0] = "0 0 0 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post2_pack2)
{
   mapTo = "pack2";
   diffuseColor[0] = "0.987479 1 0.283837 1";
   specular[0] = "0 0 0 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post2_table)
{
   mapTo = "table";
   diffuseColor[0] = "0.171223 0.0937475 0.03744 1";
   specular[0] = "0 0 0 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post2_basket)
{
   mapTo = "basket";
   diffuseColor[0] = "0.0055046 0.00229239 0.00144461 1";
   specular[0] = "0.388889 0.388889 0.388889 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post2_wooden_board)
{
   mapTo = "wooden_board";
   diffuseColor[0] = "0.580018 0.592567 0.552281 1";
   specular[0] = "0 0 0 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post3_sword)
{
   mapTo = "sword";
   diffuseColor[0] = "0.313066 0.395294 0.405647 1";
   specular[0] = "0.0245536 0.0245536 0.0245536 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post3_shield)
{
   mapTo = "shield";
   diffuseColor[0] = "0.181262 0.0667118 0.0222282 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post3_helmet_top)
{
   mapTo = "helmet_top";
   diffuseColor[0] = "0.102588 0.0454209 0 1";
   specular[0] = "0 0 0 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post3_helmet)
{
   mapTo = "helmet";
   diffuseColor[0] = "0.519995 0.460456 0 1";
   specular[0] = "0.1386 0.1386 0.1386 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post3_wooden_crate)
{
   mapTo = "wooden_crate";
   diffuseColor[0] = "0.166328 0.0889693 0.0411111 1";
   specular[0] = "0 0 0 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(merchant_post3_belt)
{
   mapTo = "belt";
   diffuseColor[0] = "0.14186 0.0366201 0 1";
   specular[0] = "0.0438597 0.0438597 0.0438597 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};
