
singleton Material(ship_wood_details_002_wood)
{
   mapTo = "wood_details_002_wood";
   specular[0] = "0.104242 0.104242 0.104242 1";
   specularPower[0] = "47";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseColor[0] = "0.0156863 0.0156863 0.0156863 1";
   diffuseMap[0] = "art/shapes/OGA/ship/wood_planks1.jpg";
};

singleton Material(ship_flag_logo_texture_JPG)
{
   mapTo = "flag_logo_texture_JPG";
   specular[0] = "0 0 0 1";
   specularPower[0] = "47";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/ship/flag.png";
};

singleton Material(ship_wood_details_wood_pla)
{
   mapTo = "wood_details_wood_pla";
   specular[0] = "0.104242 0.104242 0.104242 1";
   specularPower[0] = "47";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseColor[0] = "0.270588 0.270588 0.270588 1";
   diffuseMap[0] = "art/shapes/OGA/ship/wood_planks1.jpg";
};

singleton Material(ship_wood_001_WoodPlanksBa)
{
   mapTo = "wood_001_WoodPlanksBa";
   specular[0] = "0.034014 0.034014 0.034014 1";
   specularPower[0] = "25";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/ship/wood_planks1.jpg";
   diffuseColor[0] = "0.266667 0.266667 0.266667 1";
};

singleton Material(ship_sail_marble_jpg)
{
   mapTo = "sail_marble_jpg";
   specular[0] = "0.097826 0.097826 0.097826 1";
   specularPower[0] = "22";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/ship/marble.jpg";
   diffuseColor[0] = "0.647059 0.615686 0.541176 1";
};
