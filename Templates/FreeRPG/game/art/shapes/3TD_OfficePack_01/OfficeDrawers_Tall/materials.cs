
singleton Material(OfficeDrawersTall__TDBlackLaminate)
{
   mapTo = "_TDBlackLaminate";
   diffuseMap[0] = "3TD_BlackLaminate";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(OfficeDrawersTall__TD_Laminate_hoz)
{
   mapTo = "_TD_Laminate_hoz";
   diffuseMap[0] = "3TD_Laminate_hoz";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   specularStrength[0] = ".2";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
