
singleton Material(OfficeDesk_01_DeskLaminate)
{
   mapTo = "DeskLaminate";
   diffuseMap[0] = "art/shapes/3TD_OfficePack_01/OfficeDesk_01/3TD_Laminate_hoz";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   specularStrength[0] = ".2";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(OfficeDesk_01_DeskBlackLanimate)
{
   mapTo = "DeskBlackLanimate";
   diffuseMap[0] = "art/shapes/3TD_OfficePack_01/OfficeDesk_01/3TD_BlackLaminate";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "76";
   translucentBlendOp = "None";
   specularStrength[0] = "1.66667";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
