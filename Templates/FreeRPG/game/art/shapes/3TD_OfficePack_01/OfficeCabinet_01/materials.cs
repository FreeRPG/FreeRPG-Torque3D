
singleton Material(OfficeCabinet_01_Laminate_01)
{
   mapTo = "Laminate_01";
   diffuseMap[0] = "3TD_Laminate_hoz";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   specularStrength[0] = "0.2";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
