
singleton Material(House4Upload_Wood_001)
{
   mapTo = "Wood_001";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Wood.jpg.001.jpg";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};
