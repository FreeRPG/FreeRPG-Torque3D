
singleton Material(ChurchDoor1)
{
   mapTo = "ChurchDoor1";
   diffuseMap[0] = "ChurchDoor1";
   translucentBlendOp = "None";
};

singleton Material(church_RoofTiles3)
{
   mapTo = "RoofTiles3";
   diffuseMap[0] = "RoofTiles3";
   translucentBlendOp = "None";
};

singleton Material(church_Black)
{
   mapTo = "Black";
   diffuseMap[0] = "Black";
   translucentBlendOp = "None";
};
