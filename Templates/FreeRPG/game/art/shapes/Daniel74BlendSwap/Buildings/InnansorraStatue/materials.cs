
singleton Material(statueshrine)
{
   mapTo = "Marble";
   diffuseMap[0] = "art/shapes/physicsShapes/tex_temple_ruins_dif.dds";
   specular[0] = "0.05 0.05 0.05 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   normalMap[0] = "art/shapes/physicsShapes/tex_temple_ruins_nrm.dds";
};
